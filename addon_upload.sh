#! /bin/bash

[[ -z $* ]] && echo "Changelog not specified! Aborting." && exit 1
[[ ! -f ./tardis_extension_turquoise_prank.gma ]] && echo "File tardis_extension_turquoise_prank.gma does not exist! Aborting." && exit 2

gmpublish update -id 2787473757 -addon tardis_extension_turquoise_prank.gma -changes "$*"